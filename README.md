# Dirsyncdaemon

Simple service that syncs changes in one directory to other directories.

## Usage

```
dirsyncdaemon [source] [target1] [target2]
```

The service will check the source directory for changes every second.

If you want to also automatically remove files from the target directories, add the ```--delete``` flag.

