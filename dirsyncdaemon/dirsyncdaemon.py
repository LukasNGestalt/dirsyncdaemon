import argparse
import os
import time
import dirsync


class Submodule:
    def __init__(self, path):
        self.path = path


def dir_path(string):
    if os.path.isdir(string):
        return os.path.abspath(string) + '/'
    else:
        raise Exception(f'{string} is not a valid directory.')


def sync_paths(src, dest, delete=False):
    options = {'ignore': ['.git']}
    if delete:
        options['purge'] = True
    dirsync.sync(src, dest, 'sync', **options)


def _main():
    parser = argparse.ArgumentParser()
    parser.add_argument('source', type=dir_path)
    parser.add_argument('destinations', type=dir_path, nargs='+')
    parser.add_argument('--delete', action='store_true')
    parser.add_argument('--no-delete', dest='delete', action='store_false')
    # parser.add_argument("--git", action='store_true')
    # parser.add_argument("--no-git", dest='git', action='store_false')
    parser.set_defaults(delete=False)  # , git=False)
    args = parser.parse_args()

    source = Submodule(args.source)
    destinations = [Submodule(dest) for dest in args.destinations]

    while True:
        for dest in destinations:
            sync_paths(source.path, dest.path, delete=args.delete)
        time.sleep(1)


if __name__ == '__main__':
    _main()
